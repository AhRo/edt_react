import {inject} from '@loopback/core';
import {juggler} from '@loopback/repository';
import * as config from './mysqledt.datasource.json';

export class MysqledtDataSource extends juggler.DataSource {
  static dataSourceName = 'mysqledt';

  constructor(
    @inject('datasources.config.mysqledt', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
