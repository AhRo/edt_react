import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Period} from '../models';
import {PeriodRepository} from '../repositories';

export class PeriodController {
  constructor(
    @repository(PeriodRepository)
    public periodRepository : PeriodRepository,
  ) {}

  /*@post('/periodes', {
    responses: {
      '200': {
        description: 'Period model instance',
        content: {'application/json': {schema: {'x-ts-type': Period}}},
      },
    },
  })
  async create(@requestBody() period: Period): Promise<Period> {
    return await this.periodRepository.create(period);
  }*/

  @get('/periodes/count', {
    responses: {
      '200': {
        description: 'Period model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Period)) where?: Where,
  ): Promise<Count> {
    return await this.periodRepository.count(where);
  }

  @get('/periodes', {
    responses: {
      '200': {
        description: 'Array of Period model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: {'x-ts-type': Period}},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Period)) filter?: Filter,
  ): Promise<Period[]> {
    return await this.periodRepository.find(filter);
  }

  /*@patch('/periodes', {
    responses: {
      '200': {
        description: 'Period PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody() period: Period,
    @param.query.object('where', getWhereSchemaFor(Period)) where?: Where,
  ): Promise<Count> {
    return await this.periodRepository.updateAll(period, where);
  }*/

  @get('/periodes/{id}', {
    responses: {
      '200': {
        description: 'Period model instance',
        content: {'application/json': {schema: {'x-ts-type': Period}}},
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<Period> {
    return await this.periodRepository.findById(id);
  }

  /*@patch('/periodes/{id}', {
    responses: {
      '204': {
        description: 'Period PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody() period: Period,
  ): Promise<void> {
    await this.periodRepository.updateById(id, period);
  }

  @put('/periodes/{id}', {
    responses: {
      '204': {
        description: 'Period PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() period: Period,
  ): Promise<void> {
    await this.periodRepository.replaceById(id, period);
  }

  @del('/periodes/{id}', {
    responses: {
      '204': {
        description: 'Period DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.periodRepository.deleteById(id);
  }*/
}
