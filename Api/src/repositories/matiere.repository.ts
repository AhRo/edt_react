import {DefaultCrudRepository} from '@loopback/repository';
import {Matiere} from '../models';
import {DbedtDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class MatiereRepository extends DefaultCrudRepository<
  Matiere,
  typeof Matiere.prototype.idmat
> {
  constructor(
    @inject('datasources.dbedt') dataSource: DbedtDataSource,
  ) {
    super(Matiere, dataSource);
  }
}
