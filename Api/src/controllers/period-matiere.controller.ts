import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {PeriodMatiere} from '../models';
import {PeriodMatiereRepository} from '../repositories';

export class PeriodMatiereController {
  constructor(
    @repository(PeriodMatiereRepository)
    public periodMatiereRepository : PeriodMatiereRepository,
  ) {}

  @post('/horaires', {
    responses: {
      '200': {
        description: 'PeriodMatiere model instance',
        content: {'application/json': {schema: {'x-ts-type': PeriodMatiere}}},
      },
    },
  })
  async create(@requestBody() periodMatiere: PeriodMatiere): Promise<PeriodMatiere> {
    return await this.periodMatiereRepository.create(periodMatiere);
  }

  @get('/horaires/count', {
    responses: {
      '200': {
        description: 'PeriodMatiere model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(PeriodMatiere)) where?: Where,
  ): Promise<Count> {
    return await this.periodMatiereRepository.count(where);
  }

  @get('/horaires', {
    responses: {
      '200': {
        description: 'Array of PeriodMatiere model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: {'x-ts-type': PeriodMatiere}},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(PeriodMatiere)) filter?: Filter,
  ): Promise<PeriodMatiere[]> {
    return await this.periodMatiereRepository.find(filter);
  }

  @patch('/horaires', {
    responses: {
      '200': {
        description: 'PeriodMatiere PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody() periodMatiere: PeriodMatiere,
    @param.query.object('where', getWhereSchemaFor(PeriodMatiere)) where?: Where,
  ): Promise<Count> {
    return await this.periodMatiereRepository.updateAll(periodMatiere, where);
  }

  /*@get('/horaires/{id}', {
    responses: {
      '200': {
        description: 'PeriodMatiere model instance',
        content: {'application/json': {schema: {'x-ts-type': PeriodMatiere}}},
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<PeriodMatiere> {
    return await this.periodMatiereRepository.findById(id);
  }*/

  @patch('/horaires/{id}', {
    responses: {
      '204': {
        description: 'PeriodMatiere PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody() periodMatiere: PeriodMatiere,
  ): Promise<void> {
    await this.periodMatiereRepository.updateById(id, periodMatiere);
  }

  @put('/horaires/{id}', {
    responses: {
      '204': {
        description: 'PeriodMatiere PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() periodMatiere: PeriodMatiere,
  ): Promise<void> {
    await this.periodMatiereRepository.replaceById(id, periodMatiere);
  }

  @del('/horaires/{id}', {
    responses: {
      '204': {
        description: 'PeriodMatiere DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.periodMatiereRepository.deleteById(id);
  }
}
