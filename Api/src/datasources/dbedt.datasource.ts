import {inject} from '@loopback/core';
import {juggler} from '@loopback/repository';
import * as config from './dbedt.datasource.json';

export class DbedtDataSource extends juggler.DataSource {
  static dataSourceName = 'dbedt';

  constructor(
    @inject('datasources.config.dbedt', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
