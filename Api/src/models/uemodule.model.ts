import {Entity, model, property} from '@loopback/repository';

@model({settings: {"strict":false}})
export class Uemodule extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id_uemod?: number;

  @property({
    type: 'number',
  })
  id_form?: number;

  @property({
    type: 'string',
    default: 'module',
  })
  classif?: string;

  @property({
    type: 'string',
    required: true,
  })
  nom: string;

  @property({
    type: 'string',
    required: true,
  })
  label: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  [prop: string]: any;

  constructor(data?: Partial<Uemodule>) {
    super(data);
  }
}
