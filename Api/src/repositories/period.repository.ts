import {DefaultCrudRepository} from '@loopback/repository';
import {Period} from '../models';
import {DbedtDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class PeriodRepository extends DefaultCrudRepository<
  Period,
  typeof Period.prototype.id_period
> {
  constructor(
    @inject('datasources.dbedt') dataSource: DbedtDataSource,
  ) {
    super(Period, dataSource);
  }
}
