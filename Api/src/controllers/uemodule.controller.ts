import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Uemodule} from '../models';
import {UemoduleRepository} from '../repositories';

export class UemoduleController {
  constructor(
    @repository(UemoduleRepository)
    public uemoduleRepository : UemoduleRepository,
  ) {}

  /*
  @post('/modules', {
    responses: {
      '200': {
        description: 'Uemodule model instance',
        content: {'application/json': {schema: {'x-ts-type': Uemodule}}},
      },
    },
  })
  async create(@requestBody() uemodule: Uemodule): Promise<Uemodule> {
    return await this.uemoduleRepository.create(uemodule);
  }*/

  @get('/modules/count', {
    responses: {
      '200': {
        description: 'Uemodule model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Uemodule)) where?: Where,
  ): Promise<Count> {
    return await this.uemoduleRepository.count(where);
  }

  @get('/modules', {
    responses: {
      '200': {
        description: 'Array of Uemodule model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: {'x-ts-type': Uemodule}},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Uemodule)) filter?: Filter,
  ): Promise<Uemodule[]> {
    return await this.uemoduleRepository.find(filter);
  }

  /*@patch('/modules', {
    responses: {
      '200': {
        description: 'Uemodule PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody() uemodule: Uemodule,
    @param.query.object('where', getWhereSchemaFor(Uemodule)) where?: Where,
  ): Promise<Count> {
    return await this.uemoduleRepository.updateAll(uemodule, where);
  }*/

  @get('/modules/{id}', {
    responses: {
      '200': {
        description: 'Uemodule model instance',
        content: {'application/json': {schema: {'x-ts-type': Uemodule}}},
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<Uemodule> {
    return await this.uemoduleRepository.findById(id);
  }

  /*@patch('/modules/{id}', {
    responses: {
      '204': {
        description: 'Uemodule PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody() uemodule: Uemodule,
  ): Promise<void> {
    await this.uemoduleRepository.updateById(id, uemodule);
  }

  @put('/modules/{id}', {
    responses: {
      '204': {
        description: 'Uemodule PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() uemodule: Uemodule,
  ): Promise<void> {
    await this.uemoduleRepository.replaceById(id, uemodule);
  }

  @del('/modules/{id}', {
    responses: {
      '204': {
        description: 'Uemodule DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.uemoduleRepository.deleteById(id);
  }*/
}
