import {Entity, model, property} from '@loopback/repository';

@model({settings: {"strict":false}})
export class Matiere extends Entity {
  @property({
    type: 'number',
    required: true,
  })
  id_mod: number;

  @property({
    type: 'number',
    id: true,
    required: false,
  })
  id_mat: number;

  @property({
    type: 'string',
    required: true,
  })
  nom: string;

  @property({
    type: 'string',
    required: true,
    default: "#000000",
  })
  couleur: string;
  
  @property({
    type: 'string',
    //required: true,
    default: "",
  })
  prof: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  [prop: string]: any;

  constructor(data?: Partial<Matiere>) {
    super(data);
  }
}
