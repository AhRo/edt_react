import {Entity, model, property} from '@loopback/repository';

@model({settings: {"strict":false}})
export class Period extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id_period?: number;

  @property({
    type: 'number',
    required: true,
  })
  id_promo: number;

  @property({
    type: 'string',
    required: true,
  })
  label: string;

  @property({
    type: 'number',
    required: true,
  })
  tDeb: number;

  @property({
    type: 'number',
    required: true,
  })
  tFin: number;

  // Define well-known properties here

  // Indexer property to allow additional data
  [prop: string]: any;

  constructor(data?: Partial<Period>) {
    super(data);
  }
}
