// Uncomment these imports to begin using these cool features!

// import {inject} from '@loopback/context';


// export class HelloController {
//   constructor() {}
// }

import {get} from '@loopback/rest';

export class HelloController {
  @get('/api/v1/hello')
  hello(): string {
    return 'Hello world!';
  }
}