import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Matiere} from '../models';
import {MatiereRepository} from '../repositories';

export class MatiereController {
  constructor(
    @repository(MatiereRepository)
    public matiereRepository : MatiereRepository,
  ) {}

  @post('/matieres', {
    responses: {
      '200': {
        description: 'Matiere model instance',
        content: {'application/json': {schema: {'x-ts-type': Matiere}}},
      },
    },
  })
  async create(@requestBody() matiere: Matiere): Promise<Matiere> {
    return await this.matiereRepository.create(matiere);
  }

  @get('/matieres/count', {
    responses: {
      '200': {
        description: 'Matiere model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Matiere)) where?: Where,
  ): Promise<Count> {
    return await this.matiereRepository.count(where);
  }

  @get('/matieres', {
    responses: {
      '200': {
        description: 'Array of Matiere model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: {'x-ts-type': Matiere}},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Matiere)) filter?: Filter,
  ): Promise<Matiere[]> {
    return await this.matiereRepository.find(filter);
  }

  @patch('/matieres', {
    responses: {
      '200': {
        description: 'Matiere PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody() matiere: Matiere,
    @param.query.object('where', getWhereSchemaFor(Matiere)) where?: Where,
  ): Promise<Count> {
    return await this.matiereRepository.updateAll(matiere, where);
  }

  @get('/matieres/{id}', {
    responses: {
      '200': {
        description: 'Matiere model instance',
        content: {'application/json': {schema: {'x-ts-type': Matiere}}},
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<Matiere> {
    return await this.matiereRepository.findById(id);
  }

  @patch('/matieres/{id}', {
    responses: {
      '204': {
        description: 'Matiere PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody() matiere: Matiere,
  ): Promise<void> {
    await this.matiereRepository.updateById(id, matiere);
  }

  @put('/matieres/{id}', {
    responses: {
      '204': {
        description: 'Matiere PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() matiere: Matiere,
  ): Promise<void> {
    await this.matiereRepository.replaceById(id, matiere);
  }

  @del('/matieres/{id}', {
    responses: {
      '204': {
        description: 'Matiere DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.matiereRepository.deleteById(id);
  }
}
