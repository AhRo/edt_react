# README
## Requirements
- Pour faire tourner l'application, il faut 2 serveurs NodeJS, un possédant la dernière version de loopback 4, une base mySQL. Et un autre possédant react.
- Pour démarrer l'api, il suffit de faire `npm start` dans le dossier racine du projet loopback.

## Les fonctionnalités du site
- affichage d'un emploi du temps depuis une bdd (servi par une api)
- modification des horaires de l'emploi du temps
- déplacer en drag and drop les horaires
- inverser les horaires avec le drag and drop
- supprimer un horaire en le déplaçant vers la suppression
- calcul des totaux des horaires horizontalement (pour chaque matière) et verticalement (pour chaque période)
- enregistement automatique de l'emploi du temps
- *note : l'UX est plus agréable sous google chrome*

## Les adresses de l'api
```
GET
    /periodes (renvoie toutes périodes)
    /modules (renvoie tous les modules)
    /matieres (renvoie toutes les matières)
    /horaires?filter[where][id_period]=0&filter[where][id_matiere]=0 (en fonction de la matiere et/ou de la période (peut renvoyer []))
    (/horaires?filter[where][id_period]=0)
    (/horaires?filter[where][id_matiere]=0)
POST
    /matieres (params : id_mod, nom, couleur, prof)
    /horaires (params : id_period, id_matiere, nbH)
PUT/PATCH
    /horaires
DELETE
    /horaires
```
## Ce que nous avons ajouté depuis la soutenance
- déplacer en drag and drop les horaires
- inverser les horaires avec le drag and drop
- supprimer un horaire en le déplaçant vers la suppression
- lier le client et l'api
- enregistement automatique de l'emploi du temps
- malgré le fait d'avoir cherché comment corriger les urls et améliorer l'api, nous n'avons pas pu trouver de solutions car toutes les solutions possibles ont été modélisé sous loopback 3.x or nous utilisons loopback 4.x dont l'architecture a fondamentalement changée et dont il n'y a pour l'instant aucune modélisation modifiant la structure des filtres pour les urls.

