import {DefaultCrudRepository} from '@loopback/repository';
import {PeriodMatiere} from '../models';
import {DbedtDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class PeriodMatiereRepository extends DefaultCrudRepository<
  PeriodMatiere,
  typeof PeriodMatiere.prototype.id
> {
  constructor(
    @inject('datasources.dbedt') dataSource: DbedtDataSource,
  ) {
    super(PeriodMatiere, dataSource);
  }
}
