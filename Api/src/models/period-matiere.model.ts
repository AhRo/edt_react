import {Entity, model, property} from '@loopback/repository';

@model({settings: {"strict":false}})
export class PeriodMatiere extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  id_period: number;

  @property({
    type: 'number',
    required: true,
  })
  id_matiere: number;

  @property({
    type: 'string',
    required: false,
    default: "",
  })
  nbH: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  [prop: string]: any;

  constructor(data?: Partial<PeriodMatiere>) {
    super(data);
  }
}
