import React from 'react';
import ReactDOM from 'react-dom';
import Moment from 'react-moment';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import './index.css';
import poubelle from './poubelle.png';
import '../node_modules/bootstrap/dist/css/bootstrap.css'
var headers = {
    'Content-Type': 'application/json'
}
const axios = require('axios');

var idmatiere = 0;
var idperiode = 0;

function addHoraire(idmat, idperiod, val){
    console.log("horaire -------------------------------")
    var id=0;
    axios.get('https://nodejs-marcwood67395.codeanyapp.com//horaires?filter[where][id_period]='+idperiod+'&filter[where][id_matiere]='+idmat,
    
    )
    .then(function (response) {
        console.log(response)
        id = response.data[0].id
        console.log(id)
        axios.patch('https://nodejs-marcwood67395.codeanyapp.com/horaires/'+id,
        {
            "id_period": idperiod,
            "id_matiere": idmat,
            "nbH": val
        },
        headers
        )
        .then(function (response) {
            
            
        })
        .catch(function (error) {
            // handle error
            console.log(error.message);
            
        })
        
    })
    .catch(function (error) {
        // handle error
        console.log(error.message);
        
    })
}

function DeleteHoraire(idmat, idperiod){
    console.log("horaire -------------------------------")
    var id=0;
    axios.get('https://nodejs-marcwood67395.codeanyapp.com//horaires?filter[where][id_period]='+idperiod+'&filter[where][id_matiere]='+idmat,
    
    )
    .then(function (response) {
        console.log(response)
        id = response.data[0].id
        console.log(id)
        axios.patch('https://nodejs-marcwood67395.codeanyapp.com/horaires/'+id,
        {
            "id_period": parseInt(idperiod),
            "id_matiere": parseInt(idmat),
            "nbH": ""
        },
        headers
        )
        .then(function (response) {
            
            
        })
        .catch(function (error) {
            // handle error
            console.log(error.message);
            
        })
        
    })
    .catch(function (error) {
        // handle error
        console.log(error.message);
        
    })
}

function switchHoraire(idmat, idperiod, val, idmat2, idperiod2, val2){
    console.log("horaire -------------------------------")
    console.log(val.toString())
    console.log(val2.toString())
    console.log(idperiod2)
    console.log(idmat)
    var id=0;
    axios.get('https://nodejs-marcwood67395.codeanyapp.com//horaires?filter[where][id_period]='+idperiod+'&filter[where][id_matiere]='+idmat,
    
    )
    .then(function (response) {
        console.log(response)
        id = response.data[0].id
        console.log(id)
        axios.patch('https://nodejs-marcwood67395.codeanyapp.com/horaires/'+id,
        {
            "id_period": parseInt(idperiod),
            "id_matiere": parseInt(idmat),
            "nbH": val2.toString()
        },
        headers
        )
        .then(function (response) {
            console.log(response)
            
        })
        .catch(function (error) {
            // handle error
            console.log(error.message);
            
        })
        
    })
    .catch(function (error) {
        // handle error
        console.log(error.message);
        
    })

    axios.get('https://nodejs-marcwood67395.codeanyapp.com//horaires?filter[where][id_period]='+idperiod2+'&filter[where][id_matiere]='+idmat2,
    
    )
    .then(function (response) {
        console.log(response)
        var id2 = response.data[0].id
        console.log(id)
        axios.patch('https://nodejs-marcwood67395.codeanyapp.com/horaires/'+id2,
        {
            "id_period": parseInt(idperiod2),
            "id_matiere": parseInt(idmat2),
            "nbH": val.toString()
        },
        headers
        )
        .then(function (response) {
            console.log(response)
            
        })
        .catch(function (error) {
            // handle error
            console.log(error.message);
            
        })
        
    })
    .catch(function (error) {
        // handle error
        console.log(error.message);
        
    })

    
}


//console.log(test)
class Table extends React.Component {
    constructor(props){
        super(props)
        this.contentEditable = React.createRef();
        this.state = {
            show: false,
            valeur: this.props.value, 
            
        };
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }
    
    handleChange = (evt )=> {
        //console.log(this.state.valeur)
        //console.log("test --------------------")
        var idMat = evt.target.parentElement.attributes["data-idmat"].value;
        var idperiod = evt.target.parentElement.attributes["data-idperiod"].value;
        var val = evt.target.innerHTML;
        
        //console.log(this.state.valeur.periodesMatiere[idMat][idperiod])
        this.props.value.periodesMatiere[idMat][idperiod].nbH = val;
        this.setState({
            valeur: this.props.value
        })
        
        
        //console.log(this.state.valeur.periodesMatiere[idMat][idperiod])
    };
    handleClose() {
        this.setState({ show: false });
    }

    changeValue = (horaire) => {
        this.props.value.periodesMatiere[idmatiere][idperiode].nbH = horaire;
        addHoraire(idmatiere, idperiode, horaire);
        this.setState({
            valeur: this.props.value,
            show : false
        })
    }
    
    handleShow= (idmat, idperiod, horaire )=> {
        idmatiere = idmat;
        idperiode = idperiod;
        //console.log(horaire.target.innerHTML)
        this.setState({
            show: true
        })
    }
    
    onDragOver = (ev) => {
        ev.preventDefault();
    }

    onDragStart = (ev, idmat, idperiod, val) => {
        ev.dataTransfer.setData("idmat", idmat);
        ev.dataTransfer.setData("idperiod", idperiod);
        ev.dataTransfer.setData("val", val);
    }

    onDrop = (ev, idmat, idperiod, val) => {
        let idmatiere = ev.dataTransfer.getData("idmat")
        let idperiode = ev.dataTransfer.getData("idperiod")
        let valeur = ev.dataTransfer.getData("val")
        this.props.value.periodesMatiere[idmatiere][idperiode].nbH = val;
        this.props.value.periodesMatiere[idmat][idperiod].nbH = valeur;
        switchHoraire(idmatiere, idperiode, valeur, idmat, idperiod, val)
        this.setState({
            valeur: this.props.value
        })
    }

    onDelete = (ev) => {
        let idmatiere = ev.dataTransfer.getData("idmat")
        let idperiode = ev.dataTransfer.getData("idperiod")
        DeleteHoraire(idmatiere, idperiode)
        this.props.value.periodesMatiere[idmatiere][idperiode].nbH = "";
        this.setState({
            valeur: this.props.value
        })
    }

    render() {
        var idmod = 0;
        var Totaux = 0;

        // or the shorthand way
        var arrayTotal = [];
        
        //console.log(this.props.value.matiere)
        return (
            <div>
                

                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Changement d'horaire</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Veuillez entrer le nombre d'heure : 
                        <br></br>
                        <input type="text" id="horaire-input"></input>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary"  onClick={this.handleClose}>
                            Close
                        </Button>
                        <Button variant="primary" onClick={() => this.changeValue(document.querySelector("#horaire-input").value)}>
                            valider
                        </Button>
                    </Modal.Footer>
                </Modal>
                <table style={{width:'70%'}}>
                    <thead>
                        <tr>
                            <th >Module / Matiere</th>
                            <th>Profs</th> 
                            {this.state.valeur.periodes.map((periode) => {
                                //console.log(periode)
                                
                                return (
                                    <th>{<Moment format="DD/MM/YYYY" unix>{periode.tDeb}</Moment>} - {<Moment format="DD/MM/YYYY" unix>{periode.tFin}</Moment>}</th>
                                )
                            })}
                            
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                        {this.state.valeur.matiere.map((matiere) => {
                            
                            if(matiere.id_mod !== idmod){
                                //console.log(matiere)
                                idmod = matiere.id_mod
                                var total = 0;
                                return (
                                    <React.Fragment>
                                        <tr>
                                            <td style={{backgroundColor:'grey'}} colSpan={5}>{this.state.valeur.modules[matiere.id_mod].nom}</td>
                                        </tr>
                                        <tr className ="matiere" data-id={matiere.id_mat} data-idmodule={matiere.id_mod} style={{backgroundColor: matiere.couleur !== null ? matiere.couleur : 'white'}}>  
                                            <td>{matiere.nom}</td> 
                                            <td>{matiere.prof ? matiere.prof : ""}</td>
                                            
                                            {this.state.valeur.periodes.map((periode) => {
                                                let val = this.state.valeur.periodesMatiere[matiere.id_mat][periode.id_period].nbH;
                                                
                                                if(!isNaN(this.state.valeur.periodesMatiere[matiere.id_mat][periode.id_period].nbH) && this.state.valeur.periodesMatiere[matiere.id_mat][periode.id_period].nbH){
                                                    console.log(arrayTotal[periode.id_period])
                                                    if(arrayTotal[periode.id_period]){
                                                        arrayTotal[periode.id_period] = parseInt(arrayTotal[periode.id_period]) + parseInt(this.state.valeur.periodesMatiere[matiere.id_mat][periode.id_period].nbH);
                                                    }else{
                                                        arrayTotal[periode.id_period] = 0;
                                                        arrayTotal[periode.id_period] = parseInt(arrayTotal[periode.id_period]) + parseInt(this.state.valeur.periodesMatiere[matiere.id_mat][periode.id_period].nbH);
                                                    }
                                                    //console.log(isNaN(this.state.valeur.periodesMatiere[matiere.id_mat][periode.id_period].nbH ))
                                                    total = total + parseInt(this.state.valeur.periodesMatiere[matiere.id_mat][periode.id_period].nbH)
                                                }
                                                
                                                return (
                                                    <td className="droppable" onDrop={(e)=>this.onDrop(e,matiere.id_mat, periode.id_period, val)}  onDragOver={(e)=>this.onDragOver(e)} data-idperiod={periode.id_period} data-idmat={matiere.id_mat}>
                                                        <div className="heure" draggable onDragStart={(e)=>this.onDragStart(e,matiere.id_mat, periode.id_period, val)} onClick={(e) => this.handleShow(matiere.id_mat, periode.id_period, e)}>{this.state.valeur.periodesMatiere[matiere.id_mat][periode.id_period].nbH}</div>
                                                        {/* <ContentEditable
                                                            innerRef={this.contentEditable}
                                                            html = {html.periode}
                                                            disabled={false} 
                                                            onKeyUp={this.handleChange}
                                                            className="heure"
                                                        /> */}
                                                        
                                                    </td>
                                                )
                                            })}
                                            <td>{total}</td>
                                        </tr>
                                    </React.Fragment>
                                    
                                )
                            }
                            total = 0;
                            return (
                                <React.Fragment>
                                    <tr className ="matiere" data-id={matiere.id_mat} data-idmodule={matiere.id_mod} style={{backgroundColor: matiere.couleur !== null ? matiere.couleur : 'white'}}>  
                                        <td>{matiere.nom}</td> 
                                        <td>{matiere.prof ? matiere.prof : ""}</td>
                                        {this.state.valeur.periodes.map((periode) => {
                                            
                                            let val =  this.state.valeur.periodesMatiere[matiere.id_mat] ? this.state.valeur.periodesMatiere[matiere.id_mat][periode.id_period].nbH : "";
                                            
                                            
                                            //console.log(this.state.valeur.periodesMatiere[matiere.id_mat][periode.id_period].nbH)
                                            if(this.state.valeur.periodesMatiere[matiere.id_mat] && this.state.valeur.periodesMatiere[matiere.id_mat][periode.id_period].nbH !== ""){
                                                if(!isNaN(this.state.valeur.periodesMatiere[matiere.id_mat][periode.id_period].nbH) && this.state.valeur.periodesMatiere[matiere.id_mat][periode.id_period].nbH){
                                                    if(arrayTotal[periode.id_period]){
                                                        arrayTotal[periode.id_period] = parseInt(arrayTotal[periode.id_period]) + parseInt(this.state.valeur.periodesMatiere[matiere.id_mat][periode.id_period].nbH);
                                                    }else{
                                                        arrayTotal[periode.id_period] = 0;
                                                        arrayTotal[periode.id_period] = parseInt(arrayTotal[periode.id_period]) + parseInt(this.state.valeur.periodesMatiere[matiere.id_mat][periode.id_period].nbH);
                                                    }
                                                    total = total + parseInt(this.state.valeur.periodesMatiere[matiere.id_mat][periode.id_period].nbH)
                                                }
                                                return (
                                                    <td className="droppable" onDrop={(e)=>this.onDrop(e,matiere.id_mat, periode.id_period, val)} onDragOver={(e)=>this.onDragOver(e)} data-idperiod={periode.id_period} data-idmat={matiere.id_mat}>
                                                        <div className="heure" draggable onDragStart={(e)=>this.onDragStart(e,matiere.id_mat, periode.id_period, val)} onClick={(e) => this.handleShow(matiere.id_mat,periode.id_period, e)}>{this.state.valeur.periodesMatiere[matiere.id_mat][periode.id_period].nbH}</div>
                                                        {/* <ContentEditable
                                                            innerRef={this.contentEditable}
                                                            html = {html.periode}
                                                            disabled={false} 
                                                            className="heure"
                                                            onKeyDown={this.handleChange}
                                                        /> */}
                                                        
                                                    </td>
                                                )
                                            }else{
                                                return (
                                                    <td className="droppable" onDrop={(e)=>this.onDrop(e,matiere.id_mat, periode.id_period, val)} onDragOver={(e)=>this.onDragOver(e)} data-idperiod={periode.id_period} data-idmat={matiere.id_mat}>
                                                        <div className="heure" draggable onDragStart={(e)=>this.onDragStart(e,matiere.id_mat, periode.id_period, val)} onClick={(e) => this.handleShow(matiere.id_mat,periode.id_period, e)}>{ this.state.valeur.periodesMatiere[matiere.id_mat] ? this.state.valeur.periodesMatiere[matiere.id_mat][periode.id_period].nbH : ""}</div>
                                                        {/* <ContentEditable
                                                            innerRef={this.contentEditable}
                                                            html = {html.periode}
                                                            disabled={false} 
                                                            className="heure"
                                                            onKeyDown={this.handleChange}
                                                        /> */}
                                                    </td>
                                                )
                                            }
                                            
                                        })}
                                        <td>{total}</td>
                                    </tr>
                                </React.Fragment>
                                
                            )
                            
                        })}
                        
                        <tr>
                            <td>Total</td>
                            <td></td>
                            
                            {this.state.valeur.periodes.map((periode) => {
                                console.log(arrayTotal);
                                if(arrayTotal[periode.id_period]){
                                    Totaux = Totaux + parseInt(arrayTotal[periode.id_period])
                                }
                                
                                return (
                                    
                                    <td className="total">{arrayTotal[periode.id_period]}</td>
                                )
                            })}
                            <td>{Totaux ? Totaux : 0}</td>
                            
                        </tr>
                    </tbody>
                </table>
                <div id="trash"><img src={poubelle} onDrop={(e)=>this.onDelete(e)} onDragOver={(e)=>this.onDragOver(e)}alt="Poubelle" width="100%"></img></div>
            </div>
        );
    }
}


var tab = {}
axios.get('https://nodejs-marcwood67395.codeanyapp.com/matieres',{},headers)
.then(function (reponse) {
    // handle success
    tab['matiere'] = reponse.data
    axios.get('https://nodejs-marcwood67395.codeanyapp.com/periodes',{},headers)
    .then(function (reponse) {
        // handle success
        tab['periodes'] = reponse.data
        axios.get('https://nodejs-marcwood67395.codeanyapp.com/modules',{},headers)
        .then(function (reponse) {
            // handle success
            tab['modules'] = {}
            reponse.data.map((module) => {
                tab['modules'][module.id_uemod]= module
            })
            axios.get('https://nodejs-marcwood67395.codeanyapp.com/horaires',{},headers)
            .then(function (reponse) {
                // handle success
                tab['periodesMatiere'] = {}
                reponse.data.map((periode) => {
                    tab['periodesMatiere'][periode.id_matiere] = {}
                })
                reponse.data.map((periode) => {
                    tab['periodesMatiere'][periode.id_matiere][periode.id_period] = periode
                })
                console.log(tab)
                ReactDOM.render(
                    <Table value={tab}/>,
                    document.getElementById('root')
                );
               
            })
            .catch(function (error) {
                // handle error
                console.log(error.message);
                
            })
        })
        .catch(function (error) {
            // handle error
            console.log(error.message);
            
        })
    })
    .catch(function (error) {
        // handle error
        console.log(error.message);
        
    })
})
.catch(function (error) {
    // handle error
    console.log(error.message);
    
})



