import {DefaultCrudRepository} from '@loopback/repository';
import {Uemodule} from '../models';
import {DbedtDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class UemoduleRepository extends DefaultCrudRepository<
  Uemodule,
  typeof Uemodule.prototype.id_uemod
> {
  constructor(
    @inject('datasources.dbedt') dataSource: DbedtDataSource,
  ) {
    super(Uemodule, dataSource);
  }
}
